const url = 'https://ajax.test-danit.com/api/swapi/films';

fetch(url)
.then((response) => {
  return response.json();
})
.then((films) => {
  const movieListElement = document.getElementById('movie__list');

  films.forEach(film => {

    const { episodeId, openingCrawl, characters} = film;
    characters.forEach(item => {
      fetch(item)
      .then((response) => {
        return response.json()
      })
      .then ((characters) => {
        const { name } = characters;
        renderCharecters (name, episodeId);
      })
    })
    movieListElement.appendChild(createFilmListElement({episodeId, openingCrawl , characters }));
  });
});

function createFilmListElement(filmData) {
  const li = document.createElement("li");
  li.id = filmData.episodeId;
  li.innerHTML = `<span>${filmData.episodeId}</span> <span>${filmData.openingCrawl}</span> `;
  return li;
}

function renderCharecters ( name , id ) {
  const li = document.querySelectorAll ( 'li');
  const ul = document.createElement("ul")
  li.forEach (element => {
    if ( element.id == id ) {
      element.insertAdjacentHTML("beforeend", `<p>${name}</p>`)
    }
    element.appendChild(ul)
  })

}